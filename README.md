# About this App

This is CRUD app by using React.js typescript. The API already given, we need to fetch the APIs to get the data, create, update and delete. The UI also already given, but we can customize. This app able to called OnlinePajak Coding Challenge.

## How to run this app

If you already have git, and node.js on your machine, you just need to run
these commands:

```
git clone https://gitlab.com/hendraputu/op-coding-challenge.git
cd op-coding-challenge
op-coding-challenge
```

Once git clone is done and you are already on right directory, then run npm install.

```
npm install
```

after that, run npm start

### `npm start`

it will automatically open your default browser with url http://localhost:3000

## How to run this app by using docker

Note: I asume you already had docker installed in your system, if not, try install Docker by clicking this link: https://docs.docker.com/get-docker/

Open your terminal, and go to op-coding-challenge directory and run this command.

```
docker-compose up
```

It will take time to install the app.
Once it's done, you have to go to your browser and access this link:

```
http://localhost:3000
```
