const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = app => {
  app.use(
    "/api",
    createProxyMiddleware({
      target: "https://online-test-api.achilles.systems",
      changeOrigin: true
    })
  );
};