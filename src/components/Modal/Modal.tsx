import React, { Fragment } from "react";
import classes from "./Modal.module.css";

interface BackdropProp {
  onClose: () => void;
}
const Backdrop = (props: BackdropProp) => {
  return <div className={classes.backdrop} onClick={props.onClose} />;
};

interface ModalOverlayProp {
  children: React.ReactNode;
}
const ModalOverlay = (props: ModalOverlayProp) => {
  return (
    <div className={classes.modal}>
      <div className={classes.content}>{props.children}</div>
    </div>
  );
};

// const portalElement: HTMLElement | null = document.getElementById('overlays');

interface ModalProps {
  onClose: () => void;
  children: React.ReactNode;
}
const Modal = (props: ModalProps) => {
  return (
    <Fragment>
      <Backdrop onClose={props.onClose} />
      <ModalOverlay>{props.children}</ModalOverlay>
    </Fragment>
  );
};

export default Modal;
