import React from "react";
import style from "./Loading.module.css";

const Loading = () => {
  return (
    <div className={style.backdrop}>
      <div className={style.loading}>
        <div className={style["lds-ellipsis"]}>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  );
};

export default Loading;
