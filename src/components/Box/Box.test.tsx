import React from "react";
import { render, screen } from "@testing-library/react";
import Box from "./Box";

describe("<Box />", () => {
  it('should render Box with "hello" text', () => {
    render(<Box>hello</Box>);

    const box = screen.getByText("hello");

    expect(box).toBeInTheDocument();
  });
});
