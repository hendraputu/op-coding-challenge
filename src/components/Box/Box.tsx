import React from "react";
import style from "./Box.module.css";

interface Props {
  children: React.ReactNode;
}

const Box = (props: Props) => {
  const { children } = props;
  return <div className={style.box}>{children}</div>;
};

export default Box;
