import React from "react";
import { render, screen } from "@testing-library/react";
import Checkbox from "./Checkbox";

describe("<Checkbox />", () => {
  it("should render Checkbox uncheck", () => {
    render(
      <Checkbox
        name="checkme"
        isChecked={false}
        label="Check me!"
        handleChange={() => jest.fn()}
      />
    );

    const checkbox = screen.getByRole("checkbox");

    expect(checkbox).toBeInTheDocument();
  });

  it("should render Checkbox is checked", () => {
    render(
      <Checkbox
        name="checkme"
        isChecked={true}
        label="Check me!"
        handleChange={() => jest.fn()}
      />
    );

    const checkbox = screen.getByRole("checkbox");

    expect(checkbox).toBeInTheDocument();
  });
});
