import React from "react";

interface Props {
  name: string;
  isChecked: boolean;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  label: string;
}

const Checkbox = (props: Props) => {
  const { name, isChecked, handleChange, label } = props;

  return (
    <div>
      <input
        role="checkbox"
        id={name}
        name={name}
        type="checkbox"
        checked={isChecked}
        onChange={handleChange}
      />
      <label>{label}</label>
    </div>
  );
};

export default Checkbox;
