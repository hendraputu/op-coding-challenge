import React, { useRef } from "react";
import { IMaskInput } from "react-imask";
import style from "./TextFieldMask.module.css";

interface Props {
  name: string;
  value: string;
  pattern: string;
  placeholder: string;
  handleChange: (value: any, mask: any, event?: InputEvent) => void;
  error?: any;
}

const TextFieldMask = (props: Props) => {
  const {
    name,
    value,
    pattern,
    placeholder,
    handleChange,
    error = null,
  } = props;
  const refMask = useRef(null);

  return (
    <div className={style.inpWrapper}>
      <IMaskInput
        id={name}
        name={name}
        className={style.inp}
        pattern={pattern}
        lazy={false}
        mask={pattern}
        value={value}
        unmask={true}
        ref={refMask}
        onAccept={handleChange}
        placeholder={placeholder}
      />
      {error && error.message && (
        <div className={style["error-message"]}>{error.message}</div>
      )}
    </div>
  );
};

export default TextFieldMask;
