import React from "react";
import style from "./Radio.module.css";

interface Props {
  name: string;
  value: string;
  label: string;
  isSelected: boolean;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
const Radio = (props: Props) => {
  const { name, value, label, isSelected, handleChange } = props;
  return (
    <div className={style.radio}>
      <input
        role={"radio"}
        id={name}
        name={name}
        onChange={handleChange}
        value={value}
        type="radio"
        checked={isSelected}
      />
      <label>{label}</label>
    </div>
  );
};

export default Radio;
