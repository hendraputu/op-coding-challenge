import React from "react";
import { render, screen } from "@testing-library/react";
import Radio from "./Radio";

describe("<Radio />", () => {
  it("should render Radio uncheck", () => {
    render(
      <Radio
        name="radio1"
        value="option1"
        label="Option 1"
        isSelected={false}
        handleChange={() => jest.fn()}
      />
    );

    const radio = screen.getByRole("radio");

    expect(radio).toBeInTheDocument();
  });

  it("should render Radio is selected", () => {
    render(
      <Radio
        name="radio1"
        value="option1"
        label="Option 1"
        isSelected={true}
        handleChange={() => jest.fn()}
      />
    );

    const radio = screen.getByRole("radio");

    expect(radio).toBeInTheDocument();
  });
});
