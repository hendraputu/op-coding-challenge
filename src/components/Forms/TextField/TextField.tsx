import React, { BaseSyntheticEvent } from "react";
import style from "./TextField.module.css";

interface Props {
  name: string;
  placeholder?: string;
  value: string;
  handleChange?: (event: BaseSyntheticEvent) => void;
  required?: boolean;
  error?: any;
}

const TextField = (props: Props) => {
  const {
    name,
    placeholder = "",
    value,
    handleChange,
    required = false,
    error = null,
  } = props;

  // const onChangeHandler = (event: BaseSyntheticEvent) => {
  //   if (handleChange) {
  //     handleChange(event.target.value);
  //   }
  // };

  return (
    <div className={style.inpWrapper}>
      <input
        role={"textbox"}
        className={error && error.message ? style["inp-error"] : style.inp}
        type="text"
        id={name}
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
        required={required}
      />
      {error && error.message && (
        <div className={style["error-message"]}>{error.message}</div>
      )}
    </div>
  );
};

export default TextField;
