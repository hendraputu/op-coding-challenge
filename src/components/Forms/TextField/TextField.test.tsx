import React from "react";
import { render, screen } from "@testing-library/react";
import TextField from "./TextField";

describe("<TextField />", () => {
  it("should render TextField with no value", () => {
    render(<TextField name="test" value="" />);

    const textbox = screen.getByRole("textbox");

    expect(textbox).toBeInTheDocument();
  });

  it("should render TextField with value", () => {
    render(<TextField name="test" value="test" />);

    const textbox = screen.getByRole("textbox");

    expect(textbox).toBeInTheDocument();
  });
});
