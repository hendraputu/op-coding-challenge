import React from "react";
import { render, screen } from "@testing-library/react";
import Button from "./Button";

describe("<Button />", () => {
  it('should render Button with "Ok" text', () => {
    render(<Button>Ok</Button>);

    const button = screen.getByText("Ok");

    expect(button).toBeInTheDocument();
  });
});
