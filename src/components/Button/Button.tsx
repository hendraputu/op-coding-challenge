import React from "react";
import style from "./Button.module.css";

type variant = "text" | "contained" | "outlined";
type size = "small" | "medium" | "large";
interface ButtonProps {
  variant?: variant;
  size?: size;
  type?: "button" | "submit";
  onClick?: () => void;
  children: React.ReactNode;
}

const Button = (props: ButtonProps) => {
  const {
    variant = "contained",
    size = "medium",
    type = "button",
    onClick = () => {},
    children,
  } = props;
  return (
    <button
      type={type}
      className={`${style.btn} ${style[variant]} ${style[size]}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
