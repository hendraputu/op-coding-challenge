import React from "react";
import style from "./Alert.module.css";

export interface AlertProps {
  variant: "success" | "error";
  message: string;
  isShown: boolean;
}
const Alert = (props: AlertProps) => {
  const { variant, message, isShown } = props;

  return isShown ? (
    <div className={`${style.alert} ${style[variant]}`} role="alert">
      {message}
    </div>
  ) : (
    <div className={`${style["alert-hide"]} ${style[variant]}`} role="alert">
      {message}
    </div>
  );
};

export default Alert;
