import React from "react";
import { render, screen } from "@testing-library/react";
import Alert from "./Alert";

describe("<Alert />", () => {
  it("should render alert", () => {
    render(<Alert variant="success" message="Berhasil" isShown={true} />);

    const alert = screen.getByRole("alert");

    expect(alert).toBeInTheDocument();
  });

  it("should render alert with message", () => {
    render(<Alert variant="success" message="Berhasil" isShown={true} />);

    const alert = screen.getByText("Berhasil");

    expect(alert).toBeInTheDocument();
  });
});
