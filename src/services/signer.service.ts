import { SignerPayload } from "../types/signer.type";

interface Payload {
  data: SignerPayload;
}

const API_SIGNER = "/api/v1/signers";

export const getSigners = async () => {
  return await fetch(API_SIGNER);
};

export const postSigner = async (payload: Payload) => {
  return await fetch(API_SIGNER, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  });
};

export const putSigner = async (id: string, payload: Payload) => {
  return await fetch(`${API_SIGNER}/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  });
};

export const deleteSigner = async (id: string) => {
  return await fetch(`${API_SIGNER}/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  });
};
