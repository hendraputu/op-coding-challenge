import AddIcon from './add-icon.svg';
import EditIcon from './edit-icon.svg';
import DeleteIcon from './delete-icon.svg';

export {
    AddIcon,
    EditIcon,
    DeleteIcon
};