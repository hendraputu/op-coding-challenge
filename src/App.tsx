import React from "react";
import Signers from "./pages/Signers/Signers";

function App() {
  return <Signers />;
}

export default App;
