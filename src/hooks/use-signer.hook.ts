import { useEffect, useState } from "react";
import { Signer, SignerPayload } from "../types/signer.type";
import { FORM_INIT } from "../constants";
import { unNpwpFormat } from "../utils";
import {
  getSigners,
  postSigner,
  putSigner,
  deleteSigner,
} from "../services/signer.service";

const useSigner = () => {
  const [form, setForm] = useState<SignerPayload>({ ...FORM_INIT });
  const [signers, setSigners] = useState<Signer[]>([]);
  const [loading, setLoading] = useState(false);
  // get signers
  const retriveSigners = async () => {
    setLoading(true);
    try {
      const response = await getSigners();

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const { data } = await response.json();
      setSigners(data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      return { isSuccess: false, message: "Something went wrong!" };
    }
  };

  useEffect(() => {
    retriveSigners();
  }, []);

  // create signer
  const createSigner = async (values: SignerPayload) => {
    const npwp = unNpwpFormat(values.npwp);
    const payload = {
      data: { ...values, npwp },
    };

    setLoading(true);
    try {
      await postSigner(payload);
      await retriveSigners();
      setLoading(false);
      return { isSuccess: true, message: "Data berhasil disimpan" };
    } catch (error) {
      setLoading(false);
      return { isSuccess: false, message: "Data gagal disimpan" };
    }
  };

  // update signer
  const updateSigner = async (id: string, values: SignerPayload) => {
    const npwp = unNpwpFormat(values.npwp);
    const payload = {
      data: { ...values, npwp },
    };

    setLoading(true);
    try {
      await putSigner(id, payload);
      await retriveSigners();
      setLoading(false);
      return { isSuccess: true, message: "Data berhasil diperbaharui" };
    } catch (error) {
      setLoading(false);
      return { isSuccess: false, message: "Data gagal diperbaharui" };
    }
  };

  // delete signer
  const removeSigner = async (id: string) => {
    setLoading(true);
    try {
      await deleteSigner(id);
      await retriveSigners();
      setLoading(false);
      return { isSuccess: true, message: "Data berhasil dihapus" };
    } catch (error) {
      setLoading(false);
      return { isSuccess: false, message: "Data gagal dihapus" };
    }
  };

  return {
    form,
    signers,
    setForm,
    createSigner,
    updateSigner,
    removeSigner,
    loading,
  };
};

export default useSigner;
