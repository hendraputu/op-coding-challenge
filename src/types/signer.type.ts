export enum Signatory {
  TAXPAYER = "TAXPAYER",
  AUTHORIZED_REPRESENTATIVE = "AUTHORIZED_REPRESENTATIVE",
}

export enum StatusTaxpayer {
  ACTIVE = "ACTIVE",
  NOT_ACTIVE = "NOT_ACTIVE",
}

export interface SignerPayload {
  name: string;
  npwp: string;
  signatory: string;
  statusTaxpayer: string;
  defaultSignatory: boolean;
}
export interface Signer extends SignerPayload {
  id: string;
}

export interface Option {
  value: string;
  label: string;
}
