import {
  SignerPayload,
  Signatory,
  StatusTaxpayer,
  Option,
} from "./types/signer.type";
export const API_URL = "/api/v1/signers";

export const SignatoryOptions: Option[] = [
  {
    value: Signatory.TAXPAYER,
    label: "Wajib Pajak",
  },
  {
    value: Signatory.AUTHORIZED_REPRESENTATIVE,
    label: "Kuasa",
  },
];

export const StatusOptions: Option[] = [
  {
    value: StatusTaxpayer.ACTIVE,
    label: "Aktif",
  },
  {
    value: StatusTaxpayer.NOT_ACTIVE,
    label: "Tidak Aktif",
  },
];

export const FORM_INIT: SignerPayload = {
  name: "",
  npwp: "",
  signatory: Signatory.TAXPAYER,
  statusTaxpayer: StatusTaxpayer.ACTIVE,
  defaultSignatory: false,
};
