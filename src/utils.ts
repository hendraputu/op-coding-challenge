import { Option, SignerPayload } from "./types/signer.type";

export const getReadableFormat = (options: Option[], value: string) => {
  const option = options.find((item) => item.value === value);
  return option ? option.label : "-";
};

export const npwpFormat = (numbers: string) => {
  return numbers.replace(
    /(\d{2})(\d{3})(\d{3})(\d{1})(\d{3})(\d{3})/,
    "$1.$2.$3.$4-$5.$6"
  );
};

export const unNpwpFormat = (npwp: string) => {
  // return npwp.replace(
  //   /(\d{2})\W{1}(\d{3})\W{1}(\d{3})\W{1}(\d{1})\W{1}(\d{3})\W{1}(\d{3})/,
  //   "$1$2$3$4$5$6"
  // );
  return npwp
    .replace("-", "")
    .replaceAll(".", "")
    .replaceAll("_", "");
};

export const validateSignerForm = (values: SignerPayload) => {
  const errors: any = {};

  if (!values.name) {
    errors.name = "Required";
  }

  if (!values.npwp) {
    errors.npwp = "Required";
  } else {
    const npwpNumber = unNpwpFormat(values.npwp);
    if (npwpNumber.length < 15) {
      errors.npwp = "Must be 15 charaters";
    }
  }

  return errors;
};
