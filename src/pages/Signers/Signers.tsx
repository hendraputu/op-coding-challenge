import React, { useState, useEffect } from "react";
import Box from "../../components/Box/Box";
import Button from "../../components/Button/Button";
import SignerForm from "../SignerForm/SignerForm";
import Alert, { AlertProps } from "../../components/Alert/Alert";
import Loading from "../../components/Loading/Loading";
import { Signer, SignerPayload } from "../../types/signer.type";
import useSigner from "../../hooks/use-signer.hook";
import { getReadableFormat, npwpFormat } from "../../utils";
import { SignatoryOptions, StatusOptions, FORM_INIT } from "../../constants";
import { EditIcon, DeleteIcon } from "../../assets/icons";
import style from "./Signers.module.css";
import SignerConfirm from "./components/SignerConfirm";
import ActionButton from "./components/ActionButton";

const Signers = () => {
  const [isFormOpen, setIsFormOpen] = useState(false);
  const [isConfirm, setIsConfirm] = useState(false);
  const [selectedId, setSelectedId] = useState("");
  const [notification, setNotification] = useState<AlertProps>({
    variant: "success",
    message: "",
    isShown: false,
  });

  const {
    form,
    signers,
    setForm,
    createSigner,
    updateSigner,
    removeSigner,
    loading,
  } = useSigner();

  useEffect(() => {
    const timer = setTimeout(() => {
      setNotification({ ...notification, isShown: false });
    }, 2000);
    return () => {
      clearTimeout(timer);
    };
  }, [notification.isShown]);

  const submitHandler = async (values: SignerPayload) => {
    // event.preventDefault();
    // setForm(values);
    setIsFormOpen(false);
    if (selectedId) {
      const result = await updateSigner(selectedId, values);
      setNotification({
        variant: result.isSuccess ? "success" : "error",
        message: result.message,
        isShown: true,
      });
    } else {
      const result = await createSigner(values);
      setNotification({
        variant: result.isSuccess ? "success" : "error",
        message: result.message,
        isShown: true,
      });
    }
  };

  const addButtonHandler = () => {
    setForm({ ...FORM_INIT });
    setIsFormOpen(true);
  };

  const editButtonHandler = (signer: Signer) => {
    setSelectedId(signer.id);
    setForm(signer);
    setIsFormOpen(true);
  };

  const deleteButtonHandler = (id: string) => {
    setSelectedId(id);
    setIsConfirm(true);
  };

  const deleteHandler = async (id: string) => {
    setIsConfirm(false);
    const result = await removeSigner(id);
    setNotification({
      variant: result.isSuccess ? "success" : "error",
      message: result.message,
      isShown: true,
    });
  };

  const closeConfirmHandler = () => {
    setSelectedId("");
    setIsConfirm(false);
  };

  return (
    <section className={style.signers}>
      <h1>Penandatangan SPT</h1>
      {loading && <Loading />}
      <Box>
        <div className={style.header}>
          <h3>Tambah dan Edit daftar Penandatangan SPT Anda</h3>
          <Button onClick={addButtonHandler}>+ Tambah</Button>
        </div>
        <div className={style.content}>
          <table>
            <thead>
              <tr>
                <th>NPWP</th>
                <th>Nama</th>
                <th>Sebagai</th>
                <th>Status</th>
                <th>Default</th>
                <th>{""}</th>
              </tr>
            </thead>
            <tbody>
              {signers.map((signer) => (
                <tr key={signer.id}>
                  <td>{npwpFormat(signer.npwp)}</td>
                  <td>{signer.name}</td>
                  <td>
                    {getReadableFormat(SignatoryOptions, signer.signatory)}
                  </td>
                  <td>
                    {getReadableFormat(StatusOptions, signer.statusTaxpayer)}
                  </td>
                  <td>{signer.defaultSignatory ? "Ya" : "Tidak"}</td>
                  <td>
                    <ActionButton
                      name="Edit"
                      icon={EditIcon}
                      clickHandler={() => editButtonHandler(signer)}
                    />
                    <ActionButton
                      name="Delete"
                      icon={DeleteIcon}
                      clickHandler={() => deleteButtonHandler(signer.id)}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </Box>
      {isFormOpen && (
        <SignerForm
          form={form}
          setForm={setForm}
          onClose={setIsFormOpen}
          onSubmit={submitHandler}
        />
      )}
      {isConfirm && (
        <SignerConfirm
          id={selectedId}
          onClose={closeConfirmHandler}
          onConfirm={deleteHandler}
        />
      )}

      <Alert
        variant={notification.variant}
        message={notification.message}
        isShown={notification.isShown}
      />
    </section>
  );
};

export default Signers;
