import React from "react";
import Modal from "../../../components/Modal/Modal";
import Button from "../../../components/Button/Button";
import style from "./SignerConfirm.module.css";

interface Props {
  id: string;
  onClose: (value: boolean) => void;
  onConfirm: (id: string) => void;
}
const SignerConfirm = (props: Props) => {
  const { id, onClose, onConfirm } = props;
  return (
    <Modal onClose={() => onClose(false)}>
      <div className={style.confirm}>
        <h2>Yakin menghapus data ini?</h2>
        <footer>
          <Button variant="text" onClick={() => onClose(false)}>
            Tidak
          </Button>
          <Button onClick={() => onConfirm(id)}>Ya</Button>
        </footer>
      </div>
    </Modal>
  );
};

export default SignerConfirm;
