import React from "react";
import Button from "../../../components/Button/Button";

interface Props {
  name: string;
  icon: string;
  clickHandler: () => void;
}
const ActionButton = (props: Props) => {
  const { name, icon, clickHandler } = props;
  return (
    <Button size="small" variant="outlined" onClick={clickHandler}>
      <div style={{ display: "flex", alignItems: "center" }}>
        <img src={icon} alt={name} width={16} style={{ marginRight: "4px" }} />
        <div>{name}</div>
      </div>
    </Button>
  );
};

export default ActionButton;
