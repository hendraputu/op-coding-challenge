import React from "react";
import style from "./FieldWrapper.module.css";

interface Props {
  label: string;
  children: React.ReactNode;
}
const FieldWrapper = (props: Props) => {
  const { label, children } = props;
  return (
    <div className={style.field}>
      <label className={style["field-left"]}>{label}</label>
      <div className={style["field-right"]}>{children}</div>
    </div>
  );
};

export default FieldWrapper;
