import React from "react";
import FieldWrapper from "./FieldWrapper";
import { Radio } from "../../../components/Forms";
import { Option } from "../../../types/signer.type";

interface Props {
  name: string;
  label: string;
  data: Option[];
  selectedValue: string;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SignerRadio = (props: Props) => {
  const { name, label, data, selectedValue, handleChange } = props;
  return (
    <FieldWrapper label={label}>
      {data.map((item) => (
        <Radio
          key={item.value}
          label={item.label}
          name={name}
          value={item.value}
          isSelected={selectedValue === item.value}
          handleChange={handleChange}
        />
      ))}
    </FieldWrapper>
  );
};

export default SignerRadio;
