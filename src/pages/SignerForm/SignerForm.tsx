import React from "react";
import { useFormik } from "formik";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import { TextField, Checkbox, TextFieldMask } from "../../components/Forms";
import FieldWrapper from "./components/FieldWrapper";
import SignerRadio from "./components/SignerRadio";
import { SignerPayload } from "../../types/signer.type";
import { SignatoryOptions, StatusOptions } from "../../constants";
import { validateSignerForm } from "../../utils";
import style from "./SignerForm.module.css";

interface Props {
  form: SignerPayload;
  setForm: (form: SignerPayload) => void;
  onClose: (value: boolean) => void;
  onSubmit: (values: SignerPayload) => void;
}

const SignerForm = (props: Props) => {
  const { form, setForm, onClose, onSubmit } = props;

  const formik = useFormik({
    initialValues: { ...form },
    validate: (values) => validateSignerForm(values),
    onSubmit: (values) => {
      onSubmit(values);
    },
  });

  const handleMask = (value: any, mask: any, event?: InputEvent) => {
    setForm({ ...form, npwp: value });

    event && formik.handleChange(event);
  };

  return (
    <Modal onClose={() => onClose(false)}>
      <form className={style.form} onSubmit={formik.handleSubmit}>
        <header>
          <h3>Tambah Penandatangan SPT</h3>
        </header>
        <div className={style.container}>
          <div className={style.note}>*Field wajib diisi</div>
          <FieldWrapper label="NPWP*">
            <TextFieldMask
              name="npwp"
              value={form.npwp}
              pattern="00.000.000.0-000.000"
              placeholder="__.___.___._-___.___"
              handleChange={handleMask}
              error={{ message: formik.errors.npwp }}
            />
          </FieldWrapper>
          <FieldWrapper label="Nama Penandatangan SPT*">
            <TextField
              name="name"
              placeholder="Nama Penandatangan SPT"
              value={formik.values.name}
              handleChange={formik.handleChange}
              error={{ message: formik.errors.name }}
            />
          </FieldWrapper>
          <SignerRadio
            name="signatory"
            label="Bertindak Sebagai*"
            data={SignatoryOptions}
            selectedValue={formik.values.signatory}
            handleChange={formik.handleChange}
          />
          <SignerRadio
            name="statusTaxpayer"
            label="Status Wajib Pajak*"
            data={StatusOptions}
            selectedValue={formik.values.statusTaxpayer}
            handleChange={formik.handleChange}
          />
          <div className={style.checkbox}>
            <Checkbox
              name="defaultSignatory"
              label="Jadikan sebagai default"
              isChecked={formik.values.defaultSignatory}
              handleChange={formik.handleChange}
            />
          </div>
        </div>
        <footer>
          <Button variant="text" onClick={() => onClose(false)}>
            Batal
          </Button>
          <Button type="submit">Simpan</Button>
        </footer>
      </form>
    </Modal>
  );
};

export default SignerForm;
